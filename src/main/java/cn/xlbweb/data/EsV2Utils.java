package cn.xlbweb.data;

import cn.xlbweb.util.DateUtils;
import cn.xlbweb.util.JsonUtils;
import cn.xlbweb.util.response.TableResponse;
import cn.xlbweb.util.spring.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * EsV2Utils
 *
 * @author: wudibo
 * @since 1.0.0
 */
@Slf4j
@Component
public class EsV2Utils {

    private static RestHighLevelClient client = SpringUtils.getBean(RestHighLevelClient.class);

    /**
     * 根据索引前缀拼接日期，如不存在则创建
     * em-log-2021-01
     *
     * @param prefix
     * @return
     */
    public static String getIndexByMonth(String prefix) {
        String index = prefix + DateUtils.getNowFormat(DateUtils.YYYY_MM);
        if (!existsIndex(index)) {
            createIndex(index);
        }
        return index;
    }

    public static boolean existsIndex(String index) {
        try {
            GetIndexRequest request = new GetIndexRequest(index);
            return client.indices().exists(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("es exists index error", e);
        }
        return Boolean.FALSE;
    }

    public static boolean createIndex(String index) {
        try {
            CreateIndexRequest request = new CreateIndexRequest(index);
            CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
            return createIndexResponse.isAcknowledged();
        } catch (IOException e) {
            log.error("es create index error", e);
        }
        return Boolean.FALSE;
    }

    public static Map<String, Object> searchOne(String index, String id) {
        try {
            GetRequest request = new GetRequest(index, id);
            GetResponse response = client.get(request, RequestOptions.DEFAULT);
            return response.getSourceAsMap();
        } catch (IOException e) {
            log.error("es search one error", e);
        }
        return null;
    }

    public static Object searchOne(String index, String id, Class clazz) {
        return null;
    }

    public static List<Map<String, Object>> search(String index) {
        try {
            SearchRequest searchRequest = new SearchRequest(index);
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            List<Map<String, Object>> results = new ArrayList<>();
            for (SearchHit searchHit : searchResponse.getHits()) {
                searchHit.getSourceAsMap().put("id", searchHit.getId());
                results.add(searchHit.getSourceAsMap());
            }
            return results;
        } catch (IOException e) {
            log.error("es search error", e);
        }
        return null;
    }

    public static TableResponse search(String index, QueryBuilder queryBuilder, int page, int size) {
        try {
            SearchRequest searchRequest = new SearchRequest();
            // 索引
            searchRequest.indices(index);
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            // 查询条件
            searchSourceBuilder.query(queryBuilder);
            // 分页+排序
            searchSourceBuilder.from(page);
            searchSourceBuilder.size(size);
            searchSourceBuilder.sort("createTime", SortOrder.DESC);
            searchRequest.source(searchSourceBuilder);
            // 执行查询
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            // 解析结果
            long totalHits = searchResponse.getHits().getTotalHits().value;
            List<Map<String, Object>> results = new ArrayList<>();
            for (SearchHit searchHit : searchResponse.getHits()) {
                searchHit.getSourceAsMap().put("id", searchHit.getId());
                results.add(searchHit.getSourceAsMap());
            }
            return TableResponse.success(totalHits, results);
        } catch (IOException e) {
            log.error("es search error", e);
        }
        return null;
    }

    public static TableResponse search(String index, QueryBuilder queryBuilder, int page, int size, String sort, String sortField) {
        return null;
    }

    public static TableResponse search(String index, QueryBuilder queryBuilder, int page, int size, Class clazz) {
        return null;
    }

    public static TableResponse search(String index, QueryBuilder queryBuilder, int page, int size, String sort, String sortField, Class clazz) {
        return null;
    }

    public static boolean insert(String index, Object obj) {
        try {
            IndexRequest indexRequest = new IndexRequest(index);
            indexRequest.source(JsonUtils.toJsonString(obj), XContentType.JSON);
            IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
            if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
                return Boolean.TRUE;
            }
        } catch (IOException e) {
            log.error("es insert error", e);
        }
        return Boolean.FALSE;
    }

    public static boolean delete(String index, String id) {
        try {
            DeleteRequest request = new DeleteRequest(index, id);
            DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
            return response.status().getStatus() == 200;
        } catch (IOException e) {
            log.error("es delete error", e);
        }
        return Boolean.FALSE;
    }

    public static boolean deleteBulk(String index, String ids) {
        try {
            BulkRequest bulkRequest = new BulkRequest();
            String[] idArr = StringUtils.split(ids, ",");
            for (String id : idArr) {
                DeleteRequest deleteRequest = new DeleteRequest(index, id);
                bulkRequest.add(deleteRequest);
            }
            BulkResponse responses = client.bulk(bulkRequest, RequestOptions.DEFAULT);
            return responses.hasFailures();
        } catch (IOException e) {
            log.error("es bulk delete error", e);
        }
        return Boolean.FALSE;
    }
}
