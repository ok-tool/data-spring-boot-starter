package cn.xlbweb.data;

import cn.xlbweb.util.JsonUtils;
import cn.xlbweb.util.response.TableResponse;
import cn.xlbweb.util.spring.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * EsV1Utils
 *
 * @author: wudibo
 * @since 1.0.0
 */
@Slf4j
@Component
public class EsV1Utils {

    private static ElasticsearchRestTemplate template = SpringUtils.getBean(ElasticsearchRestTemplate.class);

    /**
     * 判断索引是否存在
     *
     * @param index
     * @return
     */
    public static boolean existsIndex(String index) {
        return template.indexOps(IndexCoordinates.of(index)).exists();
    }

    /**
     * 创建索引
     *
     * @param index
     * @return
     */
    public static boolean createIndex(String index) {
        return template.indexOps(IndexCoordinates.of(index)).create();
    }

    public static Map<String, Object> getSettings(String index) {
        return template.indexOps(IndexCoordinates.of(index)).getSettings();
    }

    public static Map<String, Object> getMapping(String index) {
        return template.indexOps(IndexCoordinates.of(index)).getMapping();
    }

    public static String getType(String index, String field) {
        Map<String, Object> mapping = getMapping(index);
        Map properties = (Map) mapping.get("properties");
        Map property = (Map) properties.get(field);
        return MapUtils.getString(property, "type");
    }

    /**
     * 分页查询
     *
     * @param queryBuilder
     * @param clazz
     * @param page
     * @param size
     * @param <T>
     * @return
     */
    public static <T> TableResponse<T> search(QueryBuilder queryBuilder, Class<T> clazz, int page, int size) {
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        // 自定义查询
        nativeSearchQueryBuilder.withQuery(queryBuilder);
        // 分页
        Pageable pageable = PageRequest.of(page - 1, size);
        nativeSearchQueryBuilder.withPageable(pageable);
        // 打印DSL语句
        printDsl(nativeSearchQueryBuilder);
        // 执行查询
        SearchHits<T> searchHits = template.search(nativeSearchQueryBuilder.build(), clazz);
        // 结果解析
        return parseHits(searchHits);
    }

    /**
     * 分页+排序查询
     *
     * @param query
     * @param clazz
     * @param page
     * @param size
     * @param sort
     * @param sortField
     * @param <T>
     * @return
     */
    public static <T> TableResponse<T> search(QueryBuilder query, Class<T> clazz, int page, int size, String sort, String sortField) {
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        // 自定义查询
        nativeSearchQueryBuilder.withQuery(query);
        // 排序+分页
        Sort.Direction direction = Sort.Direction.DESC;
        if (StringUtils.equals(sort, Sort.Direction.ASC.toString())) {
            direction = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page - 1, size, direction, sortField);
        nativeSearchQueryBuilder.withPageable(pageable);
        // 打印DSL语句
        printDsl(nativeSearchQueryBuilder);
        // 执行查询
        SearchHits<T> searchHits = template.search(nativeSearchQueryBuilder.build(), clazz);
        // 结果解析
        return parseHits(searchHits);
    }

    /**
     * 结果解析
     *
     * @param searchHits
     * @param <T>
     * @return
     */
    private static <T> TableResponse<T> parseHits(SearchHits<T> searchHits) {
        List<SearchHit<T>> searchHitList = searchHits.getSearchHits();
        List<T> result = new ArrayList<>();
        for (int i = 0; i < searchHitList.size(); i++) {
            result.add(searchHitList.get(i).getContent());
        }
        return new TableResponse(searchHits.getTotalHits(), result);
    }

    /**
     * 打印DSL语句
     *
     * @param nativeSearchQueryBuilder
     */
    private static void printDsl(NativeSearchQueryBuilder nativeSearchQueryBuilder) {
        String dsl = nativeSearchQueryBuilder.build().getQuery().toString();
        log.info("DSL语句\n{}", dsl);
    }

    /**
     * 分页+高亮搜索
     *
     * @param queryBuilder
     * @param clazz
     * @param page
     * @param size
     * @param <T>
     * @return
     */
    public static <T> TableResponse<T> search(QueryBuilder queryBuilder, Class<T> clazz, int page, int size, String highlightField) {
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        nativeSearchQueryBuilder.withQuery(queryBuilder);
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field(highlightField);
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("<span>");
        nativeSearchQueryBuilder.withHighlightBuilder(highlightBuilder);
        Pageable pageable = PageRequest.of(page - 1, size);
        nativeSearchQueryBuilder.withPageable(pageable);
        SearchHits<T> searchHits = template.search(nativeSearchQueryBuilder.build(), clazz);
        List<SearchHit<T>> searchHitList = searchHits.getSearchHits();
        List<Map<String, String>> result = new ArrayList<>();
        for (int i = 0; i < searchHitList.size(); i++) {
            SearchHit<T> searchHit = searchHitList.get(i);
            List<String> list = searchHit.getHighlightFields().get(highlightField);
            Map<String, String> mapObj = JsonUtils.toObj(searchHit.getContent(), Map.class);
            mapObj.put(highlightField, list.get(0));
            result.add(mapObj);
        }
        return new TableResponse(searchHits.getTotalHits(), result);
    }
}
