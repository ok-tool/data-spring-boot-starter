<p align="center">
    <img src="https://images.gitee.com/uploads/images/2021/0225/161730_c01590a4_1152471.png"/>
    <p align="center">
        一个常用的 NoSQL 数据库工具类 SDK！
    </p>
    <p align="center">
        <img src="https://img.shields.io/badge/jdk-1.8-brightgreen">
        <img src="https://img.shields.io/badge/maven-3.6.1-brightgreen">
        <img src="https://img.shields.io/badge/license-MulanPSL-yellowgreen">
    </p>
</p>

---

> 使用

正常引入

```xml
<dependency>
    <groupId>cn.xlbweb</groupId>
    <artifactId>data-spring-boot-starter</artifactId>
    <version>1.0.0</version>
    <exclusions>
</dependency>
```

排除掉某个依赖

```xml
<dependency>
    <groupId>cn.xlbweb</groupId>
    <artifactId>data-spring-boot-starter</artifactId>
    <version>1.0.0</version>
    <exclusions>
        <exclusion>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-redis</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```